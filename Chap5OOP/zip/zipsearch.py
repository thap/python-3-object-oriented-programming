import sys
import os
import shutil
import zipfile

class ZipReplace:
    def __init__(self, filename, search_str, replace_str):
        self.filename = filename
        self.search_str = search_str
        self.replace_str = replace_str
        self.temp_dir = "unzipped-{}".format(filename)

    def _full_filename(self, filename):
        return os.path.join(self.temp_dir, filename)

    def zip_find_replace(self):
        self.unzip_files()
        self.find_replace()
        self.zip_files()

    def unzip_files(self):
        os.mkdir(self.temp_dir)
        zip = zipfile.ZipFile(self.filename)
        try:
            zip.extractall(self.temp_dir)
        finally:
            zip.close()

    def find_replace(self):
        for filename in os.listdir(self.temp_dir):
            with open(self._full_filename(filename)) as file:
                contents = file.read()
            contents = contents.replace(self.search_str, self.replace_str)
            with open(self._full_filename(filename), "w") as file:
                file.write(contents)

    def zip_files(self):
        file = zipfile.ZipFile(self.filename, "w")
        for filename in os.listdir(self.temp_dir):
            file.write(self._full_filename(filename), filename)
        shutil.rmtree(self.temp_dir)

if __name__ == "__main__":
    ZipReplace(*sys.argv[1:4]).zip_find_replace()
