from io import StringIO, BytesIO
source_file = BytesIO(b'an oft-repeated clich\xe9')
dest_file = StringIO()

char = source_file.read(1)
while char:
    dest_file.write(char.decode("latin-1"))
    char = source_file.read(1)

print(dest_file.getvalue())
